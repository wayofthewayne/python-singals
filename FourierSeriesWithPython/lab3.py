import numpy as np 
import matplotlib.pyplot as plt
import math 

delta = 0.001 #we define delta a substianially small constant 
t = np.arange(-4,4,delta) #creating the appropraite range for time, in steps of delta 
x = np.zeros(len(t)) #intialise an empty array full of 0's
T=3; #Defining the period for the graph 

#Question 2

#defining the x(t). This simply uses the function definition 
x[np.bitwise_and(t>=0, t<1)] = 1
x[np.bitwise_and(t>=3, t<4)] = 1
x[np.bitwise_and(t<-2, t>=-3)] = 1

#Computing and printing the average energy of the above signal. (Using the formula)

a0=0

for i in t:
	if(i>=0 and i<T): #When T is within the 'limits'
	 a0 = a0 + (x[t==i]*delta) #Delta is used for normalisation 

a0 = a0/T #Diving the value by the period 

print("a0: ", a0)
print()
print("Please ignore the below complex casting warning")
print()

#Plotting the signal 
plt.figure()
plt.title("Graph of x(t)", loc = 'center') #setting graph title
plt.plot(t,x) #define the axes
plt.grid('on')
plt.xlabel('t')#label the axes
plt.ylabel('x(t)')
plt.savefig('Question 2'); #saving the graph 

#Computing and plotting the fourier spectrum of coefficient (for the given range)

k = np.arange(-50,50+1,1) #Defining the range for coefficients. Steps of 1 are used since these are of course integers 
ak = np.zeros(len(k)).astype(complex) #Complex arrays are made use of, else bad complex casting occurs 

for i in k:
  if(i != 0): #a0 abides by a different formula, computed above     
     ak[k==i] = ( (1/(1j*i*2*np.pi)) * (1-np.exp(-1j*i*(2/3)*np.pi)) ) #Storing the coefficients, computed on paper using the given equation 

ak[k==0] = a0 #Setting the a0 which was computed before 
      
plt.figure()
plt.title("Coeff of fourier spectrum", loc = 'center') #setting graph title
plt.stem(k,ak) #define the axes, stem is used to get a discrete graph 
plt.grid('on')
plt.xlabel('k')#label the axes
plt.ylabel('ak coefficients')
plt.savefig('Question 2iii'); #saving the graph 

#Question 3
#Constructing the fourier series approximation using fourier series synthesis equation 

#i)
#Using 10 harmonics 
harmonics10 = np.arange(-10,10+1,1)
ak = np.zeros(len(harmonics10)).astype(complex)

#The method to compute the coefficients is identical to the one used above 
for i in harmonics10:
	if(i == 0):
		ak[harmonics10==i] = a0
		continue;

	ak[harmonics10==i] = ( (1/(1j*i*2*np.pi)) * (1-np.exp(-1j*i*(2/3)*np.pi)) )

xt = np.zeros(len(t)).astype(complex)

for i in t:
	update_xt = 0

	for j in harmonics10: 
			currentTerm = (ak[j==harmonics10] * np.exp(1j*j*i*(2/3)*np.pi)) #We multiply each coefficient by the exponential value 
			update_xt = update_xt + currentTerm # storing the coefficients in update_xt variable 
			
	xt[i==t] = update_xt #For each i in t, we update the graph. This has the effect of making it continuous
	#and defines the graph for the range given in t.

#Plotting 
plt.figure()
plt.title("x(t) with 10 harmonics", loc = 'center') #setting graph title
plt.plot(t,xt) #define the axes, stem is used to get a discrete graph 
plt.grid('on')
plt.xlabel('t')#label the axes
plt.ylabel('x(t) fourier approximation')
plt.savefig('Question 3i'); #saving the graph 

#ii)
#Same procedure for 30 harmonics 
harmonics30 = np.arange(-30,30+1,1)
ak = np.zeros(len(harmonics30)).astype(complex)

for i in harmonics30:
	if(i == 0):
		ak[harmonics30==i] = a0
		continue;

	ak[harmonics30==i] = ( (1/(1j*i*2*np.pi)) * (1-np.exp(-1j*i*(2/3)*np.pi)) )

xt = np.zeros(len(t)).astype(complex)

for i in t:
	update_xt = 0

	for j in harmonics30: 
			currentTerm = (ak[j==harmonics30] * np.exp(1j*j*i*(2/3)*np.pi))
			update_xt = update_xt + currentTerm 
			
	xt[i==t] = update_xt

plt.figure()
plt.title("x(t) with 30 harmonics", loc = 'center') #setting graph title
plt.plot(t,xt) #define the axes, stem is used to get a discrete graph 
plt.grid('on')
plt.xlabel('t')#label the axes
plt.ylabel('x(t) fourier approximation')
plt.savefig('Question 3ii'); #saving the graph 

#iii)
#Same Procedure for 100 harmoincs 
harmonics100 = np.arange(-100,100+1,1)
ak = np.zeros(len(harmonics100)).astype(complex)

for i in harmonics100:
	if(i == 0):
		ak[harmonics100==i] = a0
		continue;

	ak[harmonics100==i] = ( (1/(1j*i*2*np.pi)) * (1-np.exp(-1j*i*(2/3)*np.pi)) )

xt = np.zeros(len(t)).astype(complex)

for i in t:
	update_xt = 0

	for j in harmonics100: 
			currentTerm = (ak[j==harmonics100] * np.exp(1j*j*i*(2/3)*np.pi))
			update_xt = update_xt + currentTerm 
			
	xt[i==t] = update_xt

plt.figure()
plt.title("x(t) with 100 harmonics", loc = 'center') #setting graph title
plt.plot(t,xt) #define the axes, stem is used to get a discrete graph 
plt.grid('on')
plt.xlabel('t')#label the axes
plt.ylabel('x(t) fourier approximation')
plt.savefig('Question 3iii'); #saving the graph 


#Question 4) Computing y(t) = x(t+1/2). The method used is identical to the above, however, the time shift property was also used.
ak = np.zeros(len(harmonics100)).astype(complex)

for i in harmonics100:
	if(i == 0):
		ak[harmonics100==i] = a0
		continue;

#The time shift property allows us to just multiply a value (np.exp(1j*i*np.pi*(1/3) in this case) to each ak 
	ak[harmonics100==i] = ( (1/(1j*i*2*np.pi)) * (1-np.exp(-1j*i*(2/3)*np.pi)) * np.exp(1j*i*np.pi*(1/3)) )

yt = np.zeros(len(t)).astype(complex)

for i in t:
	update_yt = 0

	for j in harmonics100: 
			currentTerm = (ak[j==harmonics100] * np.exp(1j*j*i*(2/3)*np.pi))
			update_yt = update_yt + currentTerm 
			
	yt[i==t] = update_yt

#Plotting 
plt.figure()
plt.title("y(t) = x(t+1/2)", loc = 'center') #setting graph title
plt.plot(t,yt) #define the axes, stem is used to get a discrete graph 
plt.grid('on')
plt.xlabel('t')#label the axes
plt.ylabel('y(t)')
plt.savefig('Question 4'); #saving the graph 