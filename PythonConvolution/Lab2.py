import numpy as np 
import matplotlib.pyplot as plt


delta = 0.001 #we define delta a substianially small constant 
t = np.arange(-4,4,delta); #creating the appropraite range for time, in steps of delta 
y = np.zeros(len(t)); #intialise an empty array full of 0's


#Q2: We will plot y(t) (pen and pencil derivation)

y[np.bitwise_and(t>=0, t<1)] = (t - ((t*t)/2))[np.bitwise_and(t>=0, t<1)] #values for t between 0<=t<1 are set to t-t^2/2 

y[np.bitwise_and(t>=1, t<2)] = 1/2; #value for t between 1<=t<2 are set to 1/2

y[np.bitwise_and(t>=2, t<3)] = ((t**2)/2 -(3*t) + 9/2)[np.bitwise_and(t>=2, t<3)]; #values for t between 2<=t<3 are set to t^2/2 -3t +9/2

#Plotting
plt.figure()
plt.title("Graph of Derived y(t)", loc = 'center') #setting graph title
plt.plot(t,y) #define the axes
plt.grid('on')
plt.xlabel('tao')#label the axes
plt.ylabel('y(t)')
plt.savefig('Question 2'); #saving the graph 
#(please note, working on a cli, I was unable to display the image)

#Q3: We will plot the convolution from scratch. It will be called y1(t)

x = np.zeros(len(t)) #construct the input signal with the appropriate values 
x[np.bitwise_and(t>=0, t<=2)] = 1


h = np.zeros(len(t)) #construct the other signal with the appropriate values 
h[np.bitwise_and(t>=0, t<=1)] = (1-t)[np.bitwise_and(t>=0, t<=1)]

x_reversed = x[::-1] #reversing the input signal

from scipy.ndimage.interpolation import shift

y1 = np.zeros(len(t)) #an array to store the convolution signal 

for i in t:	# a loop for -4 to 4 in steps of delta 
 x_shifted = shift( x_reversed ,i*(1/delta), cval =0) #The reversed signal is then shifted for each i 
 y1[t==i] = np.sum(h*x_shifted*delta) #the convolution formula is used to compute.
 #here we sum, the shifted input* the h(t) signal. Delta is used as a means of normalisation 

#plotting
plt.figure()
plt.title("Graph of y(t) through computed convolution", loc = 'center')
plt.plot(t,y1)
plt.grid('on')
plt.xlabel('time')
plt.ylabel('y1(t)')
plt.savefig('Question3.png')

#Q4: Compute the mean square error 

#plotting both figures in the same graph
plt.figure()
plt.title("Graphs for analytical (red) and computed solution (blue)", loc = 'center')
plt.plot(t,y,'r') #y is printed as red
plt.plot(t,y1,'b') #y1 is printed as blue
plt.grid('on')
plt.xlabel('time')
plt.ylabel('y(t)')
plt.savefig('TwoFiguresInOne.png')

#done using the appropraite formula, along with the corresponding python functions 
error = np.square(np.subtract(y,y1)).mean() #subtracting the two convolutions, computing their square, and taking their mean 

print (error) #displaying the error 







