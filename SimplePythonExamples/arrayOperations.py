import numpy as np 

a = np.array([1,2,3,4,5,6])
b = np.array([5,6,7,8,9,10])

c = a+b # add two array or vectors 

d = a-b # subtract two array or vectors

e = a ** 2 # raise the array to a power of 2 

f = a * a # compute the square of the array

print(a) ##printing 
print(b)
print(c)
print(d)
print(e)
print(f)

mu = np.mean(f) # computing mean 
standarddev = np.std(f) # compute standard deviation 
sum = np.sum(f) #computing sum 

print(mu) #printing 
print(standarddev)
print(sum)
