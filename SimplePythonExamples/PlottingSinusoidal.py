import numpy as np  #importing 
import matplotlib.pyplot as plt 

T=1 #setting parameters 
f_0 = 1
A=2 

#define the time distance between two samples 
delta = 0.001

#generate the time samples
t = np.arange(-2*T, 2*T, delta)

#Generate the signal 
x = A* np.cos(2*np.pi*f_0*t)

#plotting 
plt.figure()
plt.plot(t,x)
plt.grid('on')
plt.xlabel('t')
plt.ylabel('x(t)')
 # plt.show() #to display (does not work)
plt.savefig('Sinusoidal.png') # save figure 

#plotting another time domain signal u(t)

#generating an array of zeros 
u = np.zeros(len(t))

#set all elements where t>= 0 to 1 
u[t>=0] = 1 

#plot the step signal 
plt.figure()
plt.plot(t,u)
plt.grid('on')
plt.xlabel('t')
plt.ylabel('u(t)')
plt.savefig('StepSignal.png')



#We can now time reverse the signal 

#reverse the signal 
u_reversed = u[::-1]

#plotting the reverse signal 
plt.figure()
plt.plot(t, u_reversed)
plt.grid('on')
plt.xlabel('t')
plt.ylabel('u(-t)')
plt.savefig('ReverseSignal.png')

#we can then shift the time reversed step by one to the right by using the following script 

from scipy.ndimage.interpolation import shift

#shifting 
u_shifted = shift(u_reversed,1/delta, cval=1)

#plotting 
plt.figure()
plt.plot(t,u_shifted)
plt.grid('on')
plt.xlabel('t')
plt.ylabel('u(1-t)')
plt.savefig('ReverseSignalShifted.png')


#it is time to plot the symmetric pulse (function definition on lab notes)
y = np.zeros(len(t))
y[np.bitwise_and(t >= -1, t < 1)] = 0.5

#plotting the symmetric pulse signal 
plt.figure()
plt.plot(t,y)
plt.grid('on')
plt.xlabel('t')
plt.ylabel('y(t)')
plt.savefig('SymmetricPulseSignal.png')

import numpy as np 

#plotting the real part of two singals (definition in labs)

T =1
f_0 = 1
A = 2 

#define the time distance between two samples
delta = 0.001

#generate the time samples 
t =np.arange(-2*T,2*T+delta,delta)

x = (np.exp(2*np.pi*f_0*t*1j) + np.exp(-2*np.pi*f_0*t*1j))/2
print(y)

plt.figure()
plt.plot(t,x.real)
plt.grid('on')
plt.xlabel('t')
plt.ylabel('x(t)')
plt.savefig('RealPartFunction1.png')

x = (np.exp(2*np.pi*f_0*t*1j) - np.exp(-2*np.pi*f_0*t*1j))/(2*1j)
print(y)

plt.figure()
plt.plot(t,x.real)
plt.grid('on')
plt.xlabel('t')
plt.ylabel('x(t)')
plt.savefig('RealPartFunction2.png')
