import numpy as np 
import matplotlib.pyplot as plt
import math 

#Q1.1 

delta = 0.05 #using 200 samples 
t = np.arange(0,10,delta) #creating the appropraite range for time, in steps of delta 
x = np.zeros(len(t)) #intialise an empty array full of 0's

f1 =4 
f2 =7 
 
x = np.sin(2*np.pi*f1*t)+0.5*np.sin(2*np.pi*f2*t) #define the input signal 

 
plt.figure() #plotting 
plt.title("Graph of x(t)", loc = 'center')
plt.plot(t,x)
plt.grid('on')
plt.xlabel('t')
plt.ylabel('x(t)')
plt.savefig('Q1_1.png')

#Q1.3

#defining the fourier tranform function 
def fourier_transform(amplitude, samplingFrequency):
	NFFT=4056 
	fourierTransform = np.fft.fft(amplitude,NFFT)/len(amplitude)
	fourierTransform = np.fft.fftshift(fourierTransform)
	fVals = np.arange(start = -NFFT/2,stop = NFFT/2)/NFFT*samplingFrequency
	return fVals, fourierTransform



vals, fT = fourier_transform(x,delta) #function call 

plt.figure() #plotting 
plt.title("Fourier Transfrom Magnitude Response", loc = 'center')
plt.plot(vals,fT)
plt.grid('on')
plt.xlabel('Frequencies')
plt.ylabel('Magnitude Response')
plt.savefig('Q1_3.png')

#Q2 

t = np.arange(-4,6,0.01)
x = np.zeros(len(t))

x[np.bitwise_and(t>=1, t<=2)] = 1 #defining the input signal 
x[np.bitwise_and(t>=2, t<=3)] = 1.5 
x[np.bitwise_and(t>=3, t<=4)] = 1 

plt.figure() # plotting 
plt.title("Graph of x(t)", loc = 'center')
plt.plot(t,x)
plt.grid('on')
plt.xlabel('t')
plt.ylabel('x(t)')
plt.savefig('Q2.png')

#Q3 

w = np.arange(-4,6,0.01)
Hw = np.zeros(len(w))


Hw[np.bitwise_and(w>=-2.5, w<=2.5)] = 1 #defining the impulse response in the frequency domain 

plt.figure() #plotting 
plt.title("H(w)", loc = 'center')
plt.plot(w,Hw)
plt.grid('on')
plt.xlabel('w')
plt.ylabel('H(w)')
plt.savefig('Q3.png')

#freq, coeffs = fourier_transform(x,w) #computing the fourier transform 
#Yw = np.zeros(len(w)) 

 
#Yw = Hw*coeffs #using the convolution property 

#plt.figure() #plotting 
#plt.title("Y(w)", loc = 'center')
#plt.plot(w,Yw)
#plt.grid('on')
#plt.xlabel('w')
#plt.ylabel('Y(w)')
#plt.savefig('Q4.png')
